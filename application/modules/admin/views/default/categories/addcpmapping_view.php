<?php

$res_pc = $this->category_model->fetch_cp_all();
$res_all_cat = $this->category_model->fetch_category_all();
$res_all_cp_cat = $this->category_model->fetch_category_ajax();
$user_id = $this->session->userdata('user_id');

?>

<div class="row">	
    <div class="col-md-12">
          <?php echo $this->session->flashdata('msg');?>
        <div class="box">
            <div class="box-title">
                <h3><i class="fa fa-bars"></i> <?php echo lang_key('add_pc_mapping');?></h3>
                <div class="box-tool">
                    <a href="#" data-action="collapse"><i class="fa fa-chevron-up"></i></a>

                </div>
            </div>
            <div class="box-content">

                <form class="form-horizontal" id="addcategory" name="addcategory" action="<?php echo site_url('admin/category/addcpmapping');?>" method="post">
                    <div class="form-group">
                        <label class="col-sm-3 col-md-3 control-label"><?php echo lang_key('choose_category');?>:</label>
                        <div class="col-sm-4 col-md-4 controls">
                            <select class="form-control input-sm" name="choose_category" id="choose_category">
                                <option value="" style="display: none"><?php echo lang_key('category');?></option>
                                <?php $k=1; foreach ($res_all_cat as $r): ?>
                                    <option value="<?php echo $r->id; ?>" ><?php echo $r->title;?></option>
                                <?php $k++; endforeach; ?>

                            </select>
                            <?php echo form_error('choose_category'); ?>
                        </div>
                    </div>	
                    
                    <div class="form-group">
                        <label class="col-sm-3 col-md-3 control-label"><?php echo lang_key('property_name');?>:</label>
                        <div class="col-sm-4 col-md-4 controls">
                            <select class="form-control input-sm" name="property_name" id="property_name">
                                <option value="" style="display: none"><?php echo lang_key('property_name');?></option>
                                <?php $k=1; foreach ($res_all_cp_cat as $p): ?>
                                    <option value="<?php echo $p->id; ?>" ><?php echo $p->p_name;?></option>
                                <?php $k++; endforeach; ?>

                            </select>
                            <?php echo form_error('property_name'); ?>
                        </div>
                    </div>	
                    <div class="form-group">
                        <label class="col-sm-3 col-md-3 control-label">&nbsp;</label>
                        <div class="col-sm-4 col-md-4 controls">						
                            <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo lang_key('save');?></button>
                            <input type="hidden" name="add_cp" value="<?php if(isset($_GET['action']) && $_GET['action'] == 'edit'){ echo 'update'; } else { echo 'new'; } ?>">
                        </div>
                    </div><br>
                
                    <div id="no-more-tables">
                        <table id="all-posts" class="table table-hover">
                            <thead>
                                <tr>
                                   <th class="numeric">#</th>
                                   <th class="numeric"><?php echo lang_key('category_name');?></th>
                                   <th class="numeric"><?php echo lang_key('property_name');?></th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                                <?php  $i=1;foreach($res_pc as $row) { 
                                    $catagory_name = $this->category_model->get_category_by_id($row->category_id);
                                    $property_name = $this->category_model->fetch_cp_by_id($row->property_id);
                                    
                                    ?>
                                <tr>
                                    <td class="pc_td numeric"><?php echo $i;?></td>
                                    <td class="pc_td numeric"><?php echo $catagory_name->title;?></td>
                                    <td class="pc_td numeric"><?php echo $property_name[0]->p_name;?></td>

                                </tr>
                                <?php $i++; }?>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#addcategory").validate({
        rules:{
            choose_category: "required",
            property_name: "required"
        },
        messages:{
            choose_category: "Please select category name",
            property_name: "Please select property Name"
        }
    });
    
});


</script>
<style>
    .error { color: red; }
    
</style>

