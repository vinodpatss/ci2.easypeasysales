<?php

$res_pc = $this->category_model->fetch_category_ajax();
$user_id = $this->session->userdata('user_id');

$property_name = '';
$property_label = '';
$property_type = '';
$property_info = '';
$placeholder = '';
$required = '';

if (isset($_GET['action']) && $_GET['action'] != '') {
    $property_label = $_GET['property_label'];
    $property_name = $_GET['property_name'];
    $property_type = $_GET['property_type'];
    $property_info = $_GET['property_info'];
    $placeholder = $_GET['placeholder'];
    $required = $_GET['required'];
    
    
}

?>

<div class="row">	
    <div class="col-md-12">
          <?php echo $this->session->flashdata('msg');?>
        <div class="box">
            <div class="box-title">
                <h3><i class="fa fa-bars"></i> <?php echo lang_key('add_properties_for_category');?></h3>
                <div class="box-tool">
                    <a href="#" data-action="collapse"><i class="fa fa-chevron-up"></i></a>

                </div>
            </div>
            <div class="box-content">

                <form class="form-horizontal" id="addcategory" name="addcategory" action="<?php echo site_url('admin/category/addprotertiesforcategory');?>" method="post">
                    <div class="form-group">
                        <label class="col-sm-3 col-md-3 control-label"><?php echo lang_key('property_label');?>:</label>
                        <div class="col-sm-4 col-md-4 controls">
                            <input type="text" name="property_label" value="<?php echo $property_label; ?>" class="form-control input-sm" >
                            <?php echo form_error('property_label'); ?>
                        </div>
                    </div>	
                    <div class="form-group">
                        <label class="col-sm-3 col-md-3 control-label"><?php echo lang_key('property_input_type');?>:</label>
                        <div class="col-sm-4 col-md-4 controls">
                            <select class="form-control input-sm" name="property_input_type" id="property_input_type">
                                <option value="" style="display: none"><?php echo lang_key('select_type');?></option>
                                <option value="text" <?php if($property_type == 'text'){echo 'selected';} ?>><?php echo lang_key('text');?></option>
                                <option value="number" <?php if($property_type == 'number'){echo 'selected';} ?>><?php echo lang_key('number');?></option>
                                <option value="email" <?php if($property_type == 'email'){echo 'selected';} ?>><?php echo lang_key('email');?></option>
                                <option value="textarea" <?php if($property_type == 'textarea'){echo 'selected';} ?>><?php echo lang_key('textarea');?></option>
                                <option value="dropdown" <?php if($property_type == 'dropdown'){echo 'selected';} ?>><?php echo lang_key('dropdown');?></option>

                            </select>
                            <?php echo form_error('property_input_type'); ?>
                        </div>
                    </div>	
                    <div class="form-group" style="<?php if(isset($_GET['property_type'])){ echo ''; }else { echo 'display:none'; } ?>" id="info_display">
                        <label class="col-sm-3 col-md-3 control-label"><?php echo lang_key('property_info');?>:</label>
                        <div class="col-sm-4 col-md-4 controls">
                            <textarea name="property_info" class="form-control input-sm" ><?php echo $property_info; ?></textarea>
                            <?php echo form_error('property_info'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 col-md-3 control-label"><?php echo lang_key('property_name');?>:</label>
                        <div class="col-sm-4 col-md-4 controls">
                            <input type="text" name="property_name" value="<?php echo $property_name; ?>" class="form-control input-sm" >
                            <?php echo form_error('property_name'); ?>
                        </div>
                    </div>	
                    <div class="form-group">
                        <label class="col-sm-3 col-md-3 control-label"><?php echo lang_key('placeholder');?>:</label>
                        <div class="col-sm-4 col-md-4 controls">
                            <input type="text" name="placeholder" value="<?php echo $placeholder; ?>" class="form-control input-sm" >
                            <?php echo form_error('placeholder'); ?>
                        </div>
                    </div>	
                    <div class="form-group">
                        <label class="col-sm-3 col-md-3 control-label"><?php echo lang_key('required');?>:</label>
                        <div class="col-sm-1 col-md-1 controls">
                            <input type="checkbox" name="required" value="1" class="form-control input-sm" <?php if($required == 1){echo 'checked'; } ?> >
                            <?php echo form_error('required'); ?>
                        </div>
                    </div>	

                    <div class="form-group">
                        <label class="col-sm-3 col-md-3 control-label">&nbsp;</label>
                        <div class="col-sm-4 col-md-4 controls">						
                            <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo lang_key('save');?></button>
                            <input type="hidden" name="add_cp" value="<?php if(isset($_GET['action']) && $_GET['action'] == 'edit'){ echo 'update'; } else { echo 'new'; } ?>">
                        </div>
                    </div><br>
                
                    <div id="no-more-tables">
                        <table id="all-posts" class="table table-hover">
                            <thead>
                                <tr>
                                   <th class="numeric">#</th>
                                   <th class="numeric"><?php echo lang_key('property_label');?></th>
                                   <th class="numeric"><?php echo lang_key('property_type');?></th>
                                   <th class="numeric"><?php echo lang_key('placeholder');?></th>
                                   <th class="numeric"><?php echo lang_key('property_name');?></th>
                                   <th class="numeric"><?php echo lang_key('property_info');?></th>
                                   <th class="numeric"><?php echo lang_key('action');?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  $i=1;foreach($res_pc as $row): ?>
                                <tr>
                                    <td class="pc_td numeric"><?php echo $i;?></td>
                                    <td class="pc_td numeric"><?php echo $row->label;?></td>
                                    <td class="pc_td numeric"><?php echo $row->type;?></td>
                                    <td class="pc_td numeric"><?php echo $row->placeholder;?></td>
                                    <td class="pc_td numeric"><?php echo $row->p_name;?></td>
                                    <td class="pc_td numeric"><?php echo $row->info;?></td>
                                    <td data-title="<?php echo lang_key('actions');?>" class="pc_td numeric">
                                        <div class="btn-group">
                                            <a class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-cog"></i> <?php echo lang_key('action');?> <span class="caret"></span></a>
                                            <ul class="dropdown-menu dropdown-info">
                                                <li><a href="<?php echo site_url('admin/category/addprotertiesforcategory/?property_name='.$row->p_name.'&property_type='.$row->type.'&placeholder='.$row->placeholder.'&action=edit&id='.$row->id.'&property_label='.$row->label.'&required='.$row->required.'&property_info='.$row->info);?>"><?php echo lang_key('edit');?></a></li>
                                                <li><a href="<?php echo site_url('admin/category/addprotertiesforcategory/?action=delete&id='.$row->id);?>"><?php echo lang_key('delete');?></a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <?php $i++; endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function(){
    
    jQuery.validator.addMethod("noSpace", function(value, element) { 
        return value.indexOf(" ") < 0 && value != ""; 
    }, "No space please and don't leave it empty");
    
    jQuery("#addcategory").validate({
        rules:{
            property_label: "required",
            property_input_type: "required",
            property_name: {
                required: "required",
                noSpace: true
            }
        },
        messages:{
            property_label: "Please enter property label",
            property_input_type: "Please enter property input type",
            property_name: {
                required: "Please enter property name",
                noSpace: "please remove space or gaps"
            }
        }
    });
    
    jQuery('#property_input_type').change(function() {
        if(this.value != ''){
            jQuery("#info_display").css('display','block');
        }
    });
});

$(window).load(function() {
//    $("#info_display").css('display','block');
});
</script>
<style>
    .error { color: red; }
    .pc_td { 
        padding: 0px !important; 
        padding-bottom: 8px !important;
        padding-top: 8px !important;
        padding-left: 2px !important;
    }
</style>

