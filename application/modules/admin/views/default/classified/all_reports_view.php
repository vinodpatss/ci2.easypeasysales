<link href="<?php echo base_url();?>assets/datatable/dataTables.bootstrap.css" rel="stylesheet">

<div class="row">

    <div class="col-md-12">

        <div class="box">

            <div class="box-title">

                <h3><i class="fa fa-bars"></i> <?php echo lang_key('all_reports');?></h3>

                <div class="box-tool">

                    <a href="#" data-action="collapse"><i class="fa fa-chevron-up"></i></a>


                </div>

            </div>

            <div class="box-content">

                <?php $this->load->helper('text');?>

                <?php echo $this->session->flashdata('msg');?>

                <?php if($reports->num_rows()<=0){?>

                    <div class="alert alert-info"><?php echo lang_key('no_posts');?></div>

                <?php }else{?>

                    <div id="no-more-tables">

                        <table id="all-posts" class="table table-hover">

                            <thead>

                            <tr>

                                <th class="numeric">#</th>

                                <th class="numeric"><?php echo lang_key('post');?></th>

                                <th class="numeric"><?php echo lang_key('reason');?></th>

                                <th class="numeric"><?php echo lang_key('email');?></th>

                                <th class="numeric"><?php echo lang_key('comment');?></th>

                                <th class="numeric"><?php echo lang_key('actions');?></th>

                            </tr>

                            </thead>

                            <tbody>

                            <?php $i=1;foreach($reports->result() as $row):?>

                                <tr>

                                    <td data-title="#" class="numeric"><?php echo $i;?></td>
                                    <?php $post= get_post_by_unique_id($row->unique_id) ?>

                                    <td data-title="<?php echo lang_key('post');?>" class="numeric"><a href="<?php echo site_url('edit-ad/0/'.$post->id);?>"><?php echo get_post_data_by_lang($post,'title');?></a></td>

                                    <td data-title="<?php echo lang_key('reason');?>" class="numeric"><?php echo lang_key($row->reason);?></td>

                                    <td data-title="<?php echo lang_key('email');?>" class="numeric"><?php echo $row->email;?></td>

                                    <td data-title="<?php echo lang_key('comment');?>" class="numeric"><?php echo $row->comment;?></td>





                                    <td data-title="<?php echo lang_key('actions');?>" class="numeric">

                                        <div class="btn-group">

                                            <a class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-cog"></i> <?php echo lang_key('action');?> <span class="caret"></span></a>

                                            <ul class="dropdown-menu dropdown-info">


                                                <li><a href="<?php echo site_url('admin/classified/delete_report/'.$row->id);?>"><?php echo lang_key('delete');?></a></li>

                                            </ul>

                                        </div>

                                    </td>

                                </tr>

                                <?php $i++;endforeach;?>

                            </tbody>

                        </table>

                    </div>


                <?php }?>

            </div>

        </div>

    </div>

</div>
<script src="<?php echo base_url();?>assets/datatable/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/datatable/dataTables.bootstrap.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('#all-posts').dataTable();
    });
</script>