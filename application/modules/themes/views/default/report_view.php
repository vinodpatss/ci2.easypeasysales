<h3 style="text-align: center"><?php echo lang_key('state_your_reason_to_report'); ?></h3>
<br/>
<br/>
<?php

$CI         = get_instance();
$CI->load->config('classifieds');
$report_reasons  = $CI->config->item('report_reasons');

?>
<form name="report-form" action="<?php echo site_url('show/submit_report');?>" class="form-horizontal" role="form" method="post" onsubmit="return validateForm()">
    <!-- Form Group -->

    <input name="unique_id" type="hidden" value="<?php echo $unique_id ?>">
    <div class="form-group">
        <label for="password" class="col-sm-3 control-label"><?php echo lang_key('reason'); ?></label>
        <div class="col-sm-9">
            <select name="reason" class="form-control">
                <?php foreach ($report_reasons as $report_reason) {

                    ?>
                    <option value="<?php echo $report_reason; ?>"><?php echo lang_key($report_reason);?></option>
                <?php } ?>
            </select>
        </div>
    </div>

    <div class="form-group">
        <!-- Label -->
        <label for="user" class="col-sm-3 control-label"><?php echo lang_key('email'); ?></label>
        <div class="col-sm-9">
            <!-- Input -->
            <input type="text" class="form-control" name="email" placeholder="<?php echo lang_key('email'); ?>">
        </div>
    </div>

    <div class="form-group">
        <!-- Label -->
        <label for="user" class="col-sm-3 control-label"><?php echo lang_key('comment'); ?></label>
        <div class="col-sm-9">
            <!-- Input -->
            <textarea name="comment" value="" placeholder="<?php echo lang_key('comment');?>" class="form-control"></textarea>
        </div>
    </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
            <!-- Button -->
            <button type="submit" class="btn btn-red"><?php echo lang_key('submit'); ?></button>&nbsp;

        </div>
    </div>
</form>

<script type="text/javascript">
    function validateForm() {
        var x = document.forms["report-form"]["email"].value;
        if (x == null || x == "") {
            alert("Email must be filled out");
            return false;
        }
    }
</script>

