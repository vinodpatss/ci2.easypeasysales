<?php

class Custom_fields extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        is_installed(); #defined in auth helper

        if(!is_loggedin())
        {
                if(count($_POST)<=0)
                $this->session->set_userdata('req_url',current_url());
                redirect(site_url('account/trylogin'));
        }

        $this->per_page = get_per_page_value();
        $this->load->database();
        $this->load->model('user/post_model');
        $this->load->model('admin/category_model');

    }
    
    public function index() {
        
        if (isset($_POST['data']) && $_POST['data'] == 'fetch_custom_fields') {
            $this->ajax_fields();
        }
    }

    public function ajax_fields() {
        
        $cat_id = $_POST['cat_id'];
        if (isset($_POST['post_id']) && $_POST['post_id'] != '') {
            $post_id = $_POST['post_id'];
        }  else {
            $post_id = "0";
        }
        
        
        $property_ids = array();
        $properties_res = $this->category_model->get_propertyid_by_cat($cat_id);
        $properties_count = count($properties_res);
        
        if ($properties_count > 0) {
            foreach ($properties_res as $key => $row) {
                array_push($property_ids, $row->property_id);
            }
            
            $property_arr = implode(",", $property_ids);
            $query = $this->db->query("select * from dbc_category_ajax where id in({$property_arr})");
            $fields_details = $query->result();
            
            $fields_name = $this->custom_fields_array($fields_details);
            $fields_name = implode(',', $fields_name);
            echo "<input type='hidden' name='custom_field_names' id='custom_field_names' value='$fields_name'>";
            echo $this->draw_fields($fields_details,$post_id);
        }
    }
    
    function custom_fields_array($fields_details) {
        $field_arr =array();
	foreach($fields_details as $field_num=>$field){
            if($field->type=='dropdown'){
                array_push($field_arr, $field->p_name);
                
            }elseif($field->type=='text' || $field->type=='number' || $field->type=='email'){
                array_push($field_arr, $field->p_name);
            }
	}
        return $field_arr;
    }
    
    function draw_fields($fields_details,$post_id){
	$field_html ='';
	foreach($fields_details as $field_num=>$field){
            if($field->type=='dropdown'){
                $field_html .= $this->get_dropdown($field,$post_id);
            }elseif($field->type=='text' || $field->type=='number' || $field->type=='email'){
                $field_html .= $this->get_textfield($field,$post_id);
            }
	}
        return $field_html;

    }
    
    function get_textfield($field_obj,$post_id){
        
        $field_value = get_post_meta($post_id, $field_obj->p_name);
        
        if ($field_value == "n/a") {
            $field_value = "";
        }
        
	$ret_fld = "<div class='form-group price-input-holder'>"
                . "<label for='".$field_obj->p_name."' class='col-md-3 control-label'>".$field_obj->label."</label><div class='col-md-8'>"
                . "<input type='".$field_obj->type."' name='".$field_obj->p_name."' class='form-control' id='".$field_obj->p_name."'  placeholder='".$field_obj->placeholder."' value='".$field_value."'>"
                . "</div></div>";
	return $ret_fld ;
    }
    
    function get_dropdown($field_obj,$post_id){
        
        $field_value = get_post_meta($post_id, $field_obj->p_name);
        if ($field_value == "n/a") {
            $field_value = "";
        }
	$_options = explode("|",$field_obj->info);
	$ret_fld = "<div class='form-group price-input-holder'>"
                . "<label for='".$field_obj->p_name."' class='col-md-3 control-label'>".$field_obj->label."</label><div class='col-md-8'>"
                . "<select name='".$field_obj->p_name."'  id='".$field_obj->p_name."' class='form-control' >";
                foreach($_options as $opt){
                    $selected = "";
                    if ($opt == $field_value) { $selected = "selected"; }
                    $ret_fld .= "<option value='$opt' $selected >$opt</option>";
                }
	$ret_fld .= "</select></div></div>";
	return $ret_fld ;
    }
}